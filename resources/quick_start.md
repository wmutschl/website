---
title: Quick Start
---
<br />

{% include dynare_stable_version.md %}

For those who want to quickly have something running, this page explains how to
setup Dynare on [Windows](#windows) or [macOS](#macos) and to run your first
model (`.mod`) file.

To run Dynare, you have the choice between running it on MathWorks MATLAB
(proprietary) or GNU Octave (free software).

# Windows

## Using Dynare with MATLAB

 1. Make sure that MATLAB is installed on your computer.
 1. Download and install the [latest Dynare package](/download#stable)
 1. Open MATLAB
 1. Configure MATLAB for Dynare (see [the section below](#configuring-matlab-for-dynare-on-windows))
 1. Run a Dynare example in MATLAB (see [the section below](#running-and-editing-a-dynare-example-on-windows))

## Using Dynare with Octave

 1. Download and install the <i class="fas fa-external-link-alt"></i> [Octave installer](https://www.gnu.org/software/octave/download.html)
 1. Download and install the [latest Dynare package](/download#stable)
 1. Open Octave
 1. Configure Octave for Dynare (see [the section below](#configuring-octave-for-dynare-on-windows))
 1. Run a Dynare example in Octave (see [the section below](#running-and-editing-a-dynare-example-on-windows))

## Configuring MATLAB for Dynare on Windows

You have two choices. You can either use the [command window](#1-configuring-matlab-using-the-command-window-on-windows) or the [menu entries](#2-configuring-matlab-using-the-menu-entries-on-windows).

### 1. Configuring MATLAB using the Command Window on Windows

You will have to do this every time you start MATLAB, as MATLAB will not remember the setting.

 1. Click on the “Command Window”
 1. If you have Dynare installed in the standard location, type the following, replacing `x.y` with your Dynare version:
    ```
    >> addpath C:\dynare\x.y\matlab
    ```
    For example, for Dynare {{ dynare_stable_version }} type:
    ```
    >> addpath C:\dynare\{{ dynare_stable_version }}\matlab
    ```

### 2. Configuring MATLAB using the menu entries on Windows

You will only have to do this once as MATLAB will remember the setting the next time you run it.

 1. On the MATLAB `Home` tab, in the `Environment` section, click on `Set Path`
 1. Click `Add Folder...`. <i class="fas fa-exclamation-triangle"></i> **Warning: DO NOT** select `Add with Subfolders...`
 1. Select the `matlab` subdirectory of your Dynare installation. For example, if you have installed Dynare {{ dynare_stable_version }} in the standard location, select:
    ```
    C:\dynare\{{ dynare_stable_version }}\matlab
    ```
 1. Apply the setting by clicking `Save` button

## Configuring Octave for Dynare on Windows

If you have Dynare installed in the standard location type the following at the Octave command prompt, replacing `x.y` with your Dynare version:
Windows
```
>> addpath C:\dynare\x.y\matlab
```
For example, for Dynare {{ dynare_stable_version }} type:
```
>> addpath C:\dynare\{{ dynare_stable_version }}\matlab
```
If you don’t want to type this command every time you run Octave, you can put it in a file called `.octaverc` in your home directory. This file will usually be called `C:\Users\USERNAME\.octaverc`.

## Running and editing a Dynare example on Windows

 1. Create a working directory that will hold your Dynare models. For example, `C:\dynare\work`.
 1. Assuming that you are using Dynare {{ dynare_stable_version }}, copy the example model file `C:\dynare\{{ dynare_stable_version }}\examples\example1.mod` to your working directory.
 1. At the MATLAB or Octave command prompt, type the following to change the working directory:
    ```
    >> cd C:\dynare\work
    ```
 1. Then type the following to run the example model file:
    ```
    >> dynare example1
    ```
 1. You can edit the example by typing:
    ```
    >> edit example1.mod
    ```

<br />

# macOS

## Using Dynare with MATLAB

 1. Make sure that MATLAB is installed on your computer.
 1. Download and install the [latest Dynare package](/download#stable)
 1. Open MATLAB
 1. Configure MATLAB for Dynare (see [the section below](#configuring-matlab-for-dynare-on-macos))
 1. Run a Dynare example in MATLAB (see [the section below](#running-and-editing-a-dynare-example-on-macos))

## Using Dynare with Octave

 1. Install [Homebrew](https://brew.sh/)
 1. Install Dynare (and Octave) by typing the following in a terminal:
    ```
    brew install dynare
    ```
 1. Open Octave by running the following in a terminal:
    ```
    octave --gui
    ```
 1. Install some Octave add-ons from the Octave prompt (you only have to do it once)
    ```
    >> pkg install -forge io statistics control struct optim
    ```
 1. Configure Octave for Dynare (see [the section below](#configuring-octave-for-dynare-on-macos))
 1. Run a Dynare example in Octave (see [the section below](#running-and-editing-a-dynare-example-on-macos))

## Configuring MATLAB for Dynare on macOS

You have two choices. You can either use the [command window](#1-configuring-matlab-using-the-command-window-on-macos) or the [menu entries](#2-configuring-matlab-using-the-menu-entries-on-macos).

### 1. Configuring MATLAB using the Command Window on macOS

You will have to do this every time you start MATLAB, as MATLAB will not remember the setting.

 1. Click on the “Command Window”
 1. If you have Dynare installed in the standard location, type the following, replacing `x.y` with your Dynare version:
    ```
    >> addpath /Applications/Dynare/x.y/matlab
    ```
    For example, for Dynare {{ dynare_stable_version }} type:
    ```
    >> addpath /Applications/Dynare/{{ dynare_stable_version }}/matlab
    ```

### 2. Configuring MATLAB using the menu entries on macOS

You will only have to do this once as MATLAB will remember the setting the next time you run it.

 1. On the MATLAB `Home` tab, in the `Environment` section, click on `Set Path`
 1. Click `Add Folder...`. <i class="fas fa-exclamation-triangle"></i> **Warning: DO NOT** select `Add with Subfolders...`
 1. Select the `matlab` subdirectory of your Dynare installation. For example, if you have installed Dynare {{ dynare_stable_version }} in the standard location, select:
    ```
    /Applications/Dynare/{{ dynare_stable_version }}/matlab
    ```
 1. Apply the setting by clicking `Save` button

## Configuring Octave for Dynare on macOS

Type the following at the Octave command prompt:
```
>> addpath /usr/local/lib/dynare/matlab
```

If you don’t want to type this command every time you run Octave, you can put
it in a file called `.octaverc` in your home directory. This file will usually
be called `/Users/USERNAME/.octaverc`.

## Running and editing a Dynare example on macOS

 1. Create a working directory that will hold your Dynare models. For example, `/Users/USERNAME/work`.
 1. Assuming that you are using Dynare {{ dynare_stable_version }}, copy the
    example model file (either `/Applications/Dynare/{{ dynare_stable_version }}/examples/example1.mod` or `/usr/local/lib/dynare/examples/example1.mod`)
    to your working directory.
 1. At the MATLAB or Octave command prompt, type the following to change the working directory:
    ```
    >> cd /Users/USERNAME/work
    ```
 1. Then type the following to run the example model file:
    ```
    >> dynare example1
    ```
 1. You can edit the example by typing:
    ```
    >> edit example1.mod
    ```
