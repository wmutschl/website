---
layout: posts
title:  "1st Dynare workshop for advanced users"
date:   2022-06-28
categories:
  - events
tags:
  - Dynare workshop
---

The workshop will take place in person from Wednesday to Friday November
23-25 2022. The event will be hosted by the Joint Research Centre of the
European Commission, Ispra (IT).

## Goals and format of the workshop

The workshop aims at presenting, discussing, and sharing experiences regarding
advanced features and expert use of Dynare. It welcomes and stimulates exchanges
and contributions with/from the audience.

The 2022 iteration of the workshop will focus on the following topics:

- Solving and estimating non-linear models in Dynare:
  + Occasionally binding constraints: the OccBin toolkit in Dynare
  + Stochastic extended path

- Contributing to the Dynare project:
  + General principles
  + Introduction to git
  + Dynare's [git](https://git.dynare.org/Dynare/dynare/) repository: issues, debugging, pull requests.

- Roundtable: “Dealing with large-scale models used in policy institutions: lessons learned, issues, needs.”

## Audience

The workshop is addressed to advanced Dynare users with backgrounds ranging from
academia to policy institutions.

## Applications

The course will be open to a maximum of **20** qualified and selected participants.
Interested parties can apply by sending the following information to
[school@dynare.org](mailto:school@dynare.org):

- CV and a recent research paper (not necessarily using Dynare) (PDFs only)

- The [filled in attached application form](https://dynare.adjemian.eu/form-dynare-workshop-2022.docx) (saved as a PDF)

Applications should be submitted no later than August 31st, 2022. We will notify
successful applicants by September 16th, 2022 the latest.

## Fee

No fee is due. Lunches, coffee breaks, a social dinner on November 24th, and
workshop material are provided.

## Venue

European Commission, Joint Research Centre,
Ispra site
via E. Fermi 2749
21027 Ispra (VA), Italy.

## Travel and transportation

Participants will have to fund their own travel and accommodation expenses. The
organizers have pre-booked rooms in nearby recommended hotels from November 22
until November 25, 2022. The workshop will start around 9:00 on Wednesday and
finish around 17:00 on Friday.

Organizers will provide connections between the JRC and Milan Airports (Malpensa
and Linate) and the Milano Centrale Train Station on the days of arrival and
departure. We also organize two daily connections (morning and late afternoon)
between the JRC and the pre-booked hotels. There will be no connection provided
from other hotels to/from JRC.

## Details

The course is jointly organized by the Joint Research Centre and CEPREMAP.
Organizers and animators:

- Stéphane Adjemian (Univ. Maine)
- Michel Juillard (Banque de France)
- Willi Mutschler (Univ. Tübingen)
- Johannes Pfeifer (UniBW München)
- Marco Ratto (JRC)
- Sébastien Villemot (CEPREMAP).

Local organisers: Eleonora Beghetto, Roberta Cardani, Katia Colombo, Fabio Di
Dio, Lorenzo Frattarolo.

This is a laptop only workshop. Each participant is required to come with
his/her laptop with MATLAB R2014a or later and the latest stable Dynare version
installed. We will provide WiFi access, but participants shouldn’t rely on it to
access a MATLAB license server at their own institution. As an alternative to
MATLAB, it is possible to use GNU Octave (free software, compatible with MATLAB
syntax but slower).
