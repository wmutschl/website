---
layout: posts
title:  "Dynare 4.5.4 Released"
date:   2018-02-03
related: true
categories:
  - new-dynare-release
tags:
  - release notes
---

We are pleased to announce the release of Dynare 4.5.4.

This is a bugfix release.

The Windows packages are already available for download at:

http://www.dynare.org/download/dynare-stable 226

The Mac and GNU/Linux packages (for Debian and Ubuntu LTS) should follow
soon.

This release is compatible with MATLAB versions 7.5 (R2007b) to 9.3
(R2017b) and with GNU Octave versions 4.2.

Here is a list of the problems identified in version 4.5.3 and that have
been fixed in version 4.5.4:

- The `type` option of `plot_shock_decomposition` was always set to `qoq` regardless of what is specified.
- Bug in GSA when no parameter was detected below pvalue threshold.
- Various bug fixes in shock decompositions.
- Bug in reading in macro arrays passed on dynare command line via the `-D` option.
- Estimation with missing values was crashing if the `prefilter` option was used.
- Added a workaround for a difference in behaviour between Octave and MATLAB regarding the creation of function handles for functions that do not exist in the path. With Octave 4.2.1, steady state files did not work if no auxiliary variables were created.
- The `stoch_simul` command was crashing with a cryptic message if option `order=3` was used without setting `k_order_solver`.
- In cases where the prior bounds are infinite and the mode is estimated at exactly 0, no mode_check graphs were displayed.
- Parallel execution of MCMC was broken in models without auxiliary variables.
- Reading data with column names from Excel might crash.
- The multivariate Kalman smoother was crashing in case of missing data in the observations and Finf became singular.
- The `plot_shock_decomposition` command ignored various user-defined options like `fig_name`, `use_shock_groups` or `interactive` and instead used the default options.
- Nested `@#ifdef` and `@#ifndef` statements don't work in the macroprocessor.
